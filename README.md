# Golang Sample Web Application

## Compile from source

### Install required packages

```
> go get github.com/icrowley/fake
> go get github.com/mattn/go-sqlite3
> go get github.com/elazarl/go-bindata-assetfs
```

### Execute of build command.

```
> go build
```

### Launch of application.

```
> ./sample-web-app-for-webdriver.io
```

## Access web application

```
> open http://localhost:8080/
```

## Signin web application

Please insert to form.

- User: user
- Pass: password

# LISENCE

Apache License v2.0
