package main

import (
	"fmt"
	"time"
	//Debug: "github.com/davecgh/go-spew/spew"
	"database/sql"
	"github.com/icrowley/fake"
	_ "github.com/mattn/go-sqlite3"
	"log"
	"math"
	"net/http"
	"os"
	"strconv"
	"text/template"
)

const DB_FILEPATH = "./app.db"
const PAGE_SIZE = 20
const LOGIN_USER_NAME = "user"
const LOGIN_USER_PASSWORD = "password"
const TEMPLATE_PATH_INDEX = "template/index.html"
const TEMPLATE_PATH_LOGIN = "template/login.html"

type PageAssignStruct struct {
	UserList           UserList
	Page               int
	PageSizeEmptyArray []int
	UsersCount         int
	ErrorMessage       string
}

type UserList struct {
	Users []User
}

type User struct {
	Name    string
	Comment string
}

func main() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)

	http.Handle("/static/", http.StripPrefix("/static/", http.FileServer(assetFS())))
	//http.Handle("/static", http.StripPrefix("/static/", http.FileServer(AssetFileSystem{})))
	http.HandleFunc("/", index)
	http.HandleFunc("/login", login)
	http.HandleFunc("/logout", logout)
	http.HandleFunc("/create", create)
	http.HandleFunc("/initialize", initialize)

	fmt.Println("starting server...")
	err := http.ListenAndServe(":8080", nil)
	if err != nil {
		log.Fatal(err)
	}
}

func dbInitialize() (bool, error) {
	os.Remove(DB_FILEPATH)
	db, err := sql.Open("sqlite3", DB_FILEPATH)
	if err != nil {
		log.Fatal(err)
		return false, err
	}
	defer db.Close()

	sqlStmt := `
	CREATE TABLE users (
		id integer not null primary key,
		name text,
		comment text
	)
	`

	_, err = db.Exec(sqlStmt)
	if err != nil {
		log.Printf("%q: %s\n", err, sqlStmt)
		return false, err
	}

	tx, err := db.Begin()
	if err != nil {
		log.Fatal(err)
		return false, err
	}
	stmt, err := tx.Prepare("INSERT INTO users (name, comment) VALUES (?, ?)")
	if err != nil {
		log.Fatal(err)
		return false, err
	}
	defer stmt.Close()

	for i := 0; i < 100; i++ {
		_, err := stmt.Exec(fake.FullName(), fake.Word())
		if err != nil {
			log.Fatal(err)
			return false, err
		}
	}

	tx.Commit()

	return true, nil
}

func initialize(w http.ResponseWriter, r *http.Request) {
	_, err := dbInitialize()
	if err != nil {
		log.Fatal(err)
	}
}

func index(w http.ResponseWriter, r *http.Request) {
	if _, err := os.Stat(DB_FILEPATH); os.IsNotExist(err) {
		_, err = dbInitialize()
		if err != nil {
			log.Fatal(err)
			os.Exit(1)
		}
	}
	var t *template.Template
	if cookie, err := r.Cookie("logged_in"); err == http.ErrNoCookie || cookie.Value == "" {
		tmplLoginByte, err := Asset(TEMPLATE_PATH_LOGIN)
		if err != nil {
			log.Fatal(err)
			os.Exit(1)
		}
		t, err = template.New("tmpl").Parse(string(tmplLoginByte))
	} else {
		tmplIndexByte, err := Asset(TEMPLATE_PATH_INDEX)
		if err != nil {
			log.Fatal(err)
			os.Exit(1)
		}
		t, err = template.New("tmpl").Parse(string(tmplIndexByte))
	}

	r.ParseForm()
	var page int
	var err error
	if len(r.Form["page"]) < 1 {
		page = 0
	} else {
		page, err = strconv.Atoi(r.Form["page"][0])
		if err != nil {
			log.Fatal(err)
		}
	}

	u, err := getUserList(PAGE_SIZE, page*PAGE_SIZE)
	if err != nil {
		log.Fatal(err)
		os.Exit(1)
	}

	count, err := getUsersCount()
	if err != nil {
		log.Fatal(err)
		os.Exit(1)
	}

	var pageSizeArray = make([]int, int(math.Ceil(float64(count)/float64(PAGE_SIZE))))
	p := PageAssignStruct{UserList: u, Page: page, UsersCount: count, PageSizeEmptyArray: pageSizeArray}

	if errCookie, err := r.Cookie("app_message"); err == nil && errCookie.Value != "" {
		p.ErrorMessage = errCookie.Value
	}

	t.Execute(w, p)
}

func login(w http.ResponseWriter, r *http.Request) {
	var loginUserName string
	var loginPassword string

	r.ParseForm()
	cookie := http.Cookie{}
	loginUserName = r.FormValue("login_user_name")
	loginPassword = r.FormValue("login_password")

	if len(loginUserName) < 1 || len(loginPassword) < 1 {
		cookie = http.Cookie{Name: "app_message", Value: "Error!: Empty user name or password"}
		http.SetCookie(w, &cookie)
		http.Redirect(w, r, "/", http.StatusSeeOther)
		return
	}

	if loginUserName != LOGIN_USER_NAME || loginPassword != LOGIN_USER_PASSWORD {
		cookie = http.Cookie{Name: "app_message", Value: "Error!: Invalid user name or password"}
		http.SetCookie(w, &cookie)
		http.Redirect(w, r, "/", http.StatusSeeOther)
		return
	}

	cookie = http.Cookie{Name: "logged_in", Value: "ok"}
	http.SetCookie(w, &cookie)
	appMessageCookie := http.Cookie{Name: "app_message", Value: "", Expires: time.Now().Add(-100 * time.Hour)}
	http.SetCookie(w, &appMessageCookie)
	http.Redirect(w, r, "/", http.StatusSeeOther)
}

func logout(w http.ResponseWriter, r *http.Request) {
	cookie, err := r.Cookie("logged_in")

	if err != nil || cookie.Value == "" {
		http.Redirect(w, r, "/", http.StatusSeeOther)
	}

	updateCookie := http.Cookie{Name: "logged_in", Value: "", Expires: time.Now().Add(-100 * time.Hour)}
	http.SetCookie(w, &updateCookie)
	logoutCookie := http.Cookie{Name: "logout", Value: "ok"}
	http.SetCookie(w, &logoutCookie)
	http.Redirect(w, r, "/login", http.StatusSeeOther)
}

func create(w http.ResponseWriter, r *http.Request) {
	var userName string
	var userComment string

	r.ParseForm()
	userName = r.FormValue("user_name")
	userComment = r.FormValue("user_comment")
	if len(userName) < 1 || len(userComment) < 1 {
		http.Redirect(w, r, "/", http.StatusSeeOther)
		return
	}

	_, err := insertUserNameAndComment(userName, userComment)
	if err != nil {
		log.Fatal(err)
	}
	http.Redirect(w, r, "/", http.StatusSeeOther)
}

func insertUserNameAndComment(name string, comment string) (bool, error) {
	db, err := sql.Open("sqlite3", "./app.db")
	if err != nil {
		log.Fatal(err)
		return false, err
	}
	defer db.Close()

	tx, err := db.Begin()
	if err != nil {
		log.Fatal(err)
		return false, err
	}

	sqlStmt := `
	INSERT INTO users (name, comment) VALUES (?, ?)
	`

	stmt, err := tx.Prepare(sqlStmt)
	if err != nil {
		log.Fatal(err)
		return false, err
	}
	defer stmt.Close()

	_, err = stmt.Exec(name, comment)
	if err != nil {
		log.Fatal(err)
		return false, err
	}
	defer stmt.Close()
	tx.Commit()

	return true, nil
}

func getUserList(limit int, start int) (UserList, error) {
	db, err := sql.Open("sqlite3", "./app.db")
	if err != nil {
		log.Fatal(err)
		return UserList{}, err
	}
	defer db.Close()

	sqlStmt := `
	SELECT name, comment FROM users ORDER BY id DESC LIMIT ? OFFSET ?
	`
	stmt, err := db.Prepare(sqlStmt)
	if err != nil {
		log.Fatal(err)
		return UserList{}, err
	}
	defer db.Close()

	rows, err := stmt.Query(limit, start)
	if err != nil {
		log.Fatal(err)
		return UserList{}, err
	}
	defer rows.Close()

	ul := UserList{}

	for rows.Next() {
		var name string
		var comment string
		err = rows.Scan(&name, &comment)
		if err != nil {
			log.Fatal(err)
		}
		ul.Users = append(ul.Users, User{Name: name, Comment: comment})
	}

	return ul, nil
}

func getUsersCount() (int, error) {
	db, err := sql.Open("sqlite3", "./app.db")
	if err != nil {
		log.Fatal(err)
		return 0, err
	}
	defer db.Close()

	sqlStmt := `
	SELECT COUNT(id) id FROM users
	`

	row := db.QueryRow(sqlStmt)
	var id int

	err = row.Scan(&id)

	if err != nil {
		log.Fatal(err)
		return 0, err
	}

	return id, nil
}
